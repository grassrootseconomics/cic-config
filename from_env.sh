#!/bin/bash

while read l; do
	e=${!l}
	if [ ! -z $e ]; then
		echo "$l=$e"
	fi
done
