# CIC configuration

Configuration setting mechanism follows the usual order of precedence:

1. command line flags
2. environment variables
3. config files


## Directives

CIC Configuration directive keys should all satisify the regular expression `/^([A-Z]+_)([A-Z_]+)$/`

The first group defines the context of the directive, the second defines the directive itself.

For example: `HTTP_BASE_URL` refers to a "base url" in the context "http".


## Defaults

Configuration files are used to define the _full_ nomenclature of configurable directives.

Any default values in the component _should_ for sake of clarity be defined with the same key name as the configuration key. If possible, these symbols should be _constants_.

Symbol names of any unconfigurable values _should_ for sake of clarity match the regular expression `/^_[A-Z_]+$/`


## Files

The configuration files in thie repository define configuration nomenclature consistently used for same features across CIC components.

Directives are defined as entries in the `ini` file format, with individiual files for each feature.

If a component implements features that have corresponding files in this repository, then 

* configuration files with same name _must_ by part of that component's configuration
* _all_ directives in a configuration file must be present, even if its value will be empty


### Locations

All component repositories should include the directories `.config` where all configuration necessary for operation are stored.

* Daemon components _should_ use `/usr/local/etc/<componentname>` as default configuration directory localtion.
* User CLI components _should_ use the `xdg-basedir` standard for default configuration location.

Any component _must_ accept custom configuration directory locations by:

* Parsing the `-c` and `--config` command line flags.


### Component specific files

Any additional configuration files should be given descriptive names.

All _pluggable_ elements of the component _must_ use separate configuration files.


### Keys

The resulting configuration values after parsing _must_ be referenced by a key that matches the directive convention.

Example:

```
[foo]
bar_baz = 42

---

key: FOO_BAR_BAZ = 42

```


## Environment

All components should honor environment variables to take precedence over values defined in the configuration files.

Executables _should_ honor the `--env-prefix` flag and the `CONFINI_ENV_PREFIX` environment variable to define a string prefix for environment variable matching. For example: 

* Executable receives the flag `--env-prefix XYZZY` 
* An environment variable `XYZZY_FOO_BAR_BAZ` is defined
* The configuration key `FOO_BAR_BAZ` will be overwritten by the environment value `XYZZY_FOO_BAR_BAZ`


`CIC` and `CICTEST` prefixes _should_ be used respectively for production and development environments, to easily disamiguate from environment variables relevant to other components.


## Tools

### Python

Python components use the `confini` python module to assist in achieving the correct behavior.

https://pypi.org/project/confini/

Using this, configuration directory _must_ be configurable by defining the `CONFINI_DIR` environment variable.

Also the environment variable prefix _must_ be configurable by defining the `CONFINI_ENV_PREFIX` environment variable, for example:

* Environment variable `CONFINI_ENV_PREFIX` is set to `XXX`
* An environment variable `XXX_FOO_BAR_BAZ` is defined
* The configuration key `FOO_BAR_BAZ` will be overwritten by the environment value `XXX_FOO_BAR_BAZ`

